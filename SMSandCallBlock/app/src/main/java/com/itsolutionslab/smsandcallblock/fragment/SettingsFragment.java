package com.itsolutionslab.smsandcallblock.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.utility.SharedPrefUtil;

/**
 * Created by ASUS on 1/15/2016.
 */
public class SettingsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener{

    SwitchCompat switchCallBlock, switchSmsBlock, switchNotifyCallBlock, switchNotifySmsBlock;
    SharedPrefUtil sharedPrefUtil;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings,container,false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        handleItemClick();
        loadData();
    }


    private void initView(View view) {
        switchCallBlock = (SwitchCompat)view.findViewById(R.id.switchCallBlock);
        switchSmsBlock = (SwitchCompat)view.findViewById(R.id.switchSmsBlock);
        switchNotifyCallBlock = (SwitchCompat)view.findViewById(R.id.switchNotifyCallBlock);
        switchNotifySmsBlock = (SwitchCompat)view.findViewById(R.id.switchNotifySmsBlock);
    }


    private void handleItemClick() {
        switchCallBlock.setOnCheckedChangeListener(this);
        switchSmsBlock.setOnCheckedChangeListener(this);
        switchNotifyCallBlock.setOnCheckedChangeListener(this);
        switchNotifySmsBlock.setOnCheckedChangeListener(this);
    }


    private void loadData() {
        sharedPrefUtil = new SharedPrefUtil(getActivity());
        switchCallBlock.setChecked(sharedPrefUtil.getCallBlockStatus());
        switchSmsBlock.setChecked(sharedPrefUtil.getSmsBlockStatus());
        switchNotifyCallBlock.setChecked(sharedPrefUtil.getNotifyCallBlockStatus());
        switchNotifySmsBlock.setChecked(sharedPrefUtil.getNotifySmsBlockStatus());
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(buttonView.getId() == R.id.switchCallBlock) {
            sharedPrefUtil.setCallBlockStatus(isChecked);
        }
        else if(buttonView.getId() == R.id.switchSmsBlock) {
            sharedPrefUtil.setSmsBlockStatus(isChecked);
        }
        else if(buttonView.getId() == R.id.switchNotifyCallBlock) {
            sharedPrefUtil.setNotifyCallBlockStatus(isChecked);
        }
        else if(buttonView.getId() == R.id.switchNotifySmsBlock) {
            sharedPrefUtil.setNotifySmsBlockStatus(isChecked);
        }

    }
}
