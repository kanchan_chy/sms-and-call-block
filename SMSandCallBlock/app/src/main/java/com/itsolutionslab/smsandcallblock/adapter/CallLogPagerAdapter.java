package com.itsolutionslab.smsandcallblock.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.itsolutionslab.smsandcallblock.fragment.CallLogItemFragment;
import com.itsolutionslab.smsandcallblock.utility.Constants;

/**
 * Created by ASUS on 1/16/2016.
 */
public class CallLogPagerAdapter extends FragmentPagerAdapter {

    String fromOption;

    public CallLogPagerAdapter(FragmentManager fm, String fromOption) {
        super(fm);
        this.fromOption = fromOption;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle =new Bundle();
        bundle.putString("from", fromOption);
        if(position == 0) bundle.putString("type", Constants.CALL_INCOMING);
      //  else if (position == 1) bundle.putString("type", Constants.CALL_OUTGOING);
        else bundle.putString("type", Constants.CALL_MISSED);
        CallLogItemFragment fragment = new CallLogItemFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
