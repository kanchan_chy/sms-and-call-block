package com.itsolutionslab.smsandcallblock.blockerservice;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.receiver.WakeLocker;
import com.itsolutionslab.smsandcallblock.utility.Constants;
import com.itsolutionslab.smsandcallblock.utility.SharedPrefUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

/**
 * Created by user on 3/1/2016.
 */
public class CallBlockerService extends IntentService {

    String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    private ITelephony telephonyService;
    String number = "", name = "", photo = "", date = "", time = "";
    ArrayList<String>blackListNumbers = new ArrayList<>();
    ArrayList<String>blackListNames = new ArrayList<>();
    ArrayList<String>blackListPhotos = new ArrayList<>();

    DataHandler dataHandler;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public CallBlockerService() {
        super("CallBlockerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

            number = intent.getStringExtra("number");
            dataHandler = new DataHandler(this);
            HashMap<String, ArrayList<String>>hashBlackList = dataHandler.getAllCallBlackList();
            if(hashBlackList != null) {
                blackListNumbers = hashBlackList.get(Constants.NUMBER);
                blackListNames = hashBlackList.get(Constants.NAME);
                blackListPhotos = hashBlackList.get(Constants.PHOTO);
                boolean flag = false;
                for (int i=0; i<blackListNumbers.size(); i++) {
                    if(blackListNumbers.get(i).equals(number)) {
                        name = blackListNames.get(i);
                        photo = blackListPhotos.get(i);
                        flag = true;
                        break;
                    }
                }
                if(flag) {
                    try {
                        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        int callState = telephony.getCallState();
                        if(callState == TelephonyManager.CALL_STATE_RINGING) {
                            audioManager.setRingerMode(0);
                            Class c = Class.forName(telephony.getClass().getName());
                            Method m = c.getDeclaredMethod("getITelephony", new Class[0]);
                            m.setAccessible(true);
                            telephonyService = (ITelephony) m.invoke(telephony, new Object[0]);
                            //telephonyService.silenceRinger();
                            telephonyService.endCall();
                            loadIntoDatabase();
                            audioManager.setRingerMode(2);
                            SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(this);
                            boolean notifyCallBlockStatus = sharedPrefUtil.getNotifyCallBlockStatus();
                            if(notifyCallBlockStatus) {
                                sendNotification();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }

        }
        catch (Exception e) {

        }
       // WakeLocker.release();
        stopSelf();
    }

    private void loadIntoDatabase() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int status = calendar.get(Calendar.AM_PM);
        String am_Pm_status;
        if(status == 0) am_Pm_status = "AM";
        else am_Pm_status = "PM";
        date = months[month] + " " + day + ", " + year;
        time = hour + ":" + minute + " " + am_Pm_status;
        dataHandler.addCallBlockList(name, number, photo, date, time);
    }

    private void sendNotification() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(this, notification);
        r.play();

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String msg = "XXXXXXXXXXX January 31, 2016";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("SMS Blocked")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setAutoCancel(true);

        // mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(100, mBuilder.build());
        Toast.makeText(this, "Notification showed", Toast.LENGTH_SHORT).show();
    }

}
