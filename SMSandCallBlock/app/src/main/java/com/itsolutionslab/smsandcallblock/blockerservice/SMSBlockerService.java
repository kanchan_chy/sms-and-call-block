package com.itsolutionslab.smsandcallblock.blockerservice;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.receiver.SMSReceiver;
import com.itsolutionslab.smsandcallblock.receiver.WakeLocker;
import com.itsolutionslab.smsandcallblock.utility.Constants;
import com.itsolutionslab.smsandcallblock.utility.SharedPrefUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

/**
 * Created by user on 3/1/2016.
 */
public class SMSBlockerService extends IntentService {

    String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    String number = "", name = "", photo = "", date = "", time = "";

    DataHandler dataHandler;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public SMSBlockerService() {
        super("SMSBlockerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            number = intent.getStringExtra("number");
            name = intent.getStringExtra("name");
            photo = intent.getStringExtra("photo");
            dataHandler = new DataHandler(this);
            loadIntoDatabase();
            SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(this);
            boolean notifySmsBlockStatus = sharedPrefUtil.getNotifySmsBlockStatus();
            if(notifySmsBlockStatus) {
                sendNotification();
            }
        }
        catch (Exception e) {

        }
        //WakeLocker.release();
        stopSelf();
    }

    private void loadIntoDatabase() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int status = calendar.get(Calendar.AM_PM);
        String am_Pm_status;
        if(status == 0) am_Pm_status = "AM";
        else am_Pm_status = "PM";
        date = months[month] + " " + day + ", " + year;
        time = hour + ":" + minute + " " + am_Pm_status;
        dataHandler.addSmsBlockList(name, number, photo, date, time);
    }

    private void sendNotification() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.icon);

        String value;
        if(!name.equals("")) value = name;
        else value = number;
        String msg = "SMS blocked from " + value + " at " +date + " " + time +".";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(notificationLargeIconBitmap)
                .setContentTitle("SMS and Call Blocker")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setAutoCancel(true);

        // mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(new Random().nextInt(), mBuilder.build());
    }

}
