package com.itsolutionslab.smsandcallblock;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.itsolutionslab.smsandcallblock.adapter.InboxNumbersAdapter;

/**
 * Created by ASUS on 1/17/2016.
 */
public class InboxNumbersActivity extends AppCompatActivity{

    RecyclerView recyclerContactList;

    InboxNumbersAdapter adapter;

    String fromOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        initView();
        setAdapter();
    }

    private void initView() {
        Toolbar toolBar=(Toolbar)findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Inbox Contacts");

        recyclerContactList=(RecyclerView)findViewById(R.id.recyclerContactList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerContactList.setLayoutManager(layoutManager);

        fromOption = getIntent().getExtras().getString("from");
    }

    private void setAdapter() {
        adapter = new InboxNumbersAdapter(this, fromOption);
        recyclerContactList.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
