package com.itsolutionslab.smsandcallblock.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.AppData;
import com.itsolutionslab.smsandcallblock.utility.Constants;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 10/18/2015.
 */
public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactListViewHolder>{

    public static Activity mContext;
    String fromOption;
    ArrayList<String>names=new ArrayList<>();
    ArrayList<String>numbers=new ArrayList<>();
    ArrayList<Bitmap>photos=new ArrayList<>();

    ArrayList<String>blackListNumbers = new ArrayList<>();
    ArrayList<Boolean>isAdded = new ArrayList<>();
    DataHandler dataHandler;

    Bitmap defaultBitmap;

    public ContactListAdapter(Activity mContext, String fromOption)
    {
        this.mContext = mContext;
        this.fromOption = fromOption;
        names = AppData.getContactListNames();
        numbers = AppData.getContactListNumbers();
        photos = AppData.getContactListPhotos();
        isAdded = new ArrayList<>();

        for (int i=0; i<numbers.size(); i++) {
            isAdded.add(false);
        }
        try {
            defaultBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.user);
            dataHandler = new DataHandler(mContext);
            HashMap<String, ArrayList<String>> hashBlackList;
            if(fromOption.equals(Constants.OPTION_CALL)) {
                hashBlackList = dataHandler.getAllCallBlackList();
            }
            else {
                hashBlackList = dataHandler.getAllSmsBlackList();
            }
            if(hashBlackList != null) {
                blackListNumbers = hashBlackList.get(Constants.NUMBER);
            }
        }
        catch (Exception e) {

        }
    }

    @Override
    public ContactListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater=LayoutInflater.from(mContext);
        return new ContactListViewHolder(mInflater.inflate(R.layout.row_contact_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactListViewHolder holder, int pos) {
        holder.txtTitle.setText(names.get(pos));
        holder.txtExtra.setText(numbers.get(pos));
        if(photos.get(pos) != null) holder.imgContact.setImageBitmap(photos.get(pos));
        else holder.imgContact.setImageBitmap(defaultBitmap);
        String fullNumber = numbers.get(pos);
        if(blackListNumbers.contains(fullNumber)) {
            isAdded.set(pos, true);
            holder.imgBtnAdd.setImageResource(R.drawable.close_circular);
        }
        else {
            isAdded.set(pos, false);
            holder.imgBtnAdd.setImageResource(R.drawable.add_circular);
        }
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public class ContactListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView imgContact;
        public TextView txtTitle,txtExtra;
        public ImageButton imgBtnAdd;

        public ContactListViewHolder(View view)
        {
            super(view);
            imgContact=(ImageView)view.findViewById(R.id.imgContact);
            txtTitle=(TextView)view.findViewById(R.id.txtTitle);
            txtExtra=(TextView)view.findViewById(R.id.txtExtra);
            imgBtnAdd = (ImageButton) view.findViewById(R.id.imgBtnAdd);

            imgBtnAdd.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            if(isAdded.get(pos)) {
                if(fromOption.equals(Constants.OPTION_CALL)) {
                    dataHandler.deleteCallBlackList(numbers.get(pos));
                }
                else {
                    dataHandler.deleteSmsBlackList(numbers.get(pos));
                }
                String fullNumber = numbers.get(pos);
                blackListNumbers.remove(fullNumber);
                isAdded.set(pos, false);
                imgBtnAdd.setImageResource(R.drawable.add_circular);
            }
            else {
                String tempPhoto = "";
                if(photos.get(pos) != null) {
                    tempPhoto = BitMapToString(photos.get(pos));
                }
                if(fromOption.equals(Constants.OPTION_CALL)) {
                    dataHandler.addCallBlackList(names.get(pos), numbers.get(pos), tempPhoto);
                }
                else {
                    dataHandler.addSmsBlackList(names.get(pos), numbers.get(pos), tempPhoto);
                }
                String fullNumber = numbers.get(pos);
                blackListNumbers.add(fullNumber);
                isAdded.set(pos, true);
                imgBtnAdd.setImageResource(R.drawable.close_circular);
            }
        }
    }


    public String BitMapToString(Bitmap bitmap){
        try {
            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b=baos.toByteArray();
            String temp= Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        }
        catch (Exception e1) {
            Toast.makeText(mContext, ""+e1, Toast.LENGTH_SHORT).show();
            return "";
        }
    }



}
