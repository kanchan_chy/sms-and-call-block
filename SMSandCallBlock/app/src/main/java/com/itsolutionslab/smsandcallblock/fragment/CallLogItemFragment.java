package com.itsolutionslab.smsandcallblock.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.adapter.CallLogNumbersAdapter;

/**
 * Created by ASUS on 1/15/2016.
 */
public class CallLogItemFragment extends Fragment {

    RecyclerView recyclerLogList;

    CallLogNumbersAdapter adapter;

    String callType;
    String fromOption;
    boolean created = false;
    boolean adapterSetted = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_call_log_item,container,false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        created = true;
        adapterSetted= false;
    }

    @Override
    public void setArguments(Bundle args) {
        callType = args.getString("type");
        fromOption = args.getString("from");
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!adapterSetted) {
            setAdapter();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            if(created) {
                setAdapter();
            }
        }
    }

    private void initView(View view) {
        recyclerLogList=(RecyclerView)view.findViewById(R.id.recyclerLogList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerLogList.setLayoutManager(layoutManager);
    }

    private void setAdapter() {
        adapter = new CallLogNumbersAdapter(getActivity(), callType, fromOption);
        recyclerLogList.setAdapter(adapter);
        adapterSetted = true;
    }

}
