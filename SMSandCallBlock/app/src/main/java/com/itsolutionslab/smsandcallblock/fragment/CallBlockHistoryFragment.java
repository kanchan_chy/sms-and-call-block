package com.itsolutionslab.smsandcallblock.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.adapter.BlackListAdapter;
import com.itsolutionslab.smsandcallblock.adapter.BlockListAdapter;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 1/15/2016.
 */
public class CallBlockHistoryFragment extends Fragment {

    static TextView txtEmpty;
    RecyclerView recyclerBlockList;

    BlockListAdapter adapter;

    DataHandler dataHandler;

    ArrayList<String> names=new ArrayList<>();
    ArrayList<String>numbers=new ArrayList<>();
    ArrayList<String>photos=new ArrayList<>();
    ArrayList<String>dates=new ArrayList<>();
    ArrayList<String>times=new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_call_block_history,container,false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtEmpty=(TextView)view.findViewById(R.id.txtEmpty);
        recyclerBlockList=(RecyclerView)view.findViewById(R.id.recyclerBlockList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerBlockList.setLayoutManager(layoutManager);

        dataHandler = new DataHandler(getActivity());

    }


    @Override
    public void onResume() {
        super.onResume();
        prepareListItems();
        if(numbers.size()>0){
            txtEmpty.setVisibility(View.GONE);
            adapter=new BlockListAdapter(getActivity(), names, numbers, photos, dates, times, Constants.OPTION_CALL);
            recyclerBlockList.setAdapter(adapter);
        }
        else {
            showEmptyView();
        }
    }


    public static void showEmptyView() {
        txtEmpty.setVisibility(View.VISIBLE);
    }


    private void prepareListItems()
    {
        names=new ArrayList<>();
        numbers=new ArrayList<>();
        photos = new ArrayList<>();
        dates=new ArrayList<>();
        times=new ArrayList<>();
        try {
            HashMap<String, ArrayList<String>> hashBlockList = dataHandler.getAllCallBlockList();
            if(hashBlockList != null) {
                names = hashBlockList.get(Constants.NAME);
                numbers = hashBlockList.get(Constants.NUMBER);
                photos = hashBlockList.get(Constants.PHOTO);
                dates = hashBlockList.get(Constants.DATE);
                times = hashBlockList.get(Constants.TIME);
            }
        }
        catch (Exception e1) {

        }
    }


}
