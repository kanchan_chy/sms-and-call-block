package com.itsolutionslab.smsandcallblock;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.itsolutionslab.smsandcallblock.utility.Constants;

/**
 * Created by ASUS on 1/17/2016.
 */
public class AddBlackListActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnContact, btnLog, btnSms, btnManually;

    String fromOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_black_list);
        initView();
        setUiClickListener();
    }

    private void initView() {
        Toolbar toolBar=(Toolbar)findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setTitle("Boring Contact");

        btnContact = (Button) findViewById(R.id.btnContact);
        btnLog = (Button) findViewById(R.id.btnLog);
        btnSms = (Button) findViewById(R.id.btnSMS);
        btnManually = (Button) findViewById(R.id.btnManually);

        fromOption = getIntent().getExtras().getString("from");
        if(fromOption.equals(Constants.OPTION_CALL)) getSupportActionBar().setTitle("Boring Contact for Call");
        else getSupportActionBar().setTitle("Boring Contact for SMS");
    }

    private void setUiClickListener() {
        btnContact.setOnClickListener(this);
        btnLog.setOnClickListener(this);
        btnSms.setOnClickListener(this);
        btnManually.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnContact){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(AddBlackListActivity.this, ContactListActivity.class);
                    intent.putExtra("from", fromOption);
                    startActivity(intent);
                }
            }, 100);
        }
        else if(view.getId()==R.id.btnLog){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(AddBlackListActivity.this, LogNumbersActivity.class);
                    intent.putExtra("from", fromOption);
                    startActivity(intent);
                }
            }, 100);
        }
        else if(view.getId()==R.id.btnSMS){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(AddBlackListActivity.this, InboxNumbersActivity.class);
                    intent.putExtra("from", fromOption);
                    startActivity(intent);
                }
            }, 100);
        }
        else if(view.getId()==R.id.btnManually){

        }
    }

}
