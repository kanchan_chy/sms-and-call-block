package com.itsolutionslab.smsandcallblock.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.adapter.BlockHistoryPagerAdapter;

/**
 * Created by ASUS on 1/15/2016.
 */
public class BlockHistoryFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener{

    RelativeLayout relativeCall,relativeSMS;
    TextView txtCallIndicator,txtSMSIndicator,txtCall,txtSMS;

    ViewPager pager;
    BlockHistoryPagerAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_block_history,container,false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        relativeCall=(RelativeLayout)view.findViewById(R.id.relativeCall);
        relativeSMS=(RelativeLayout)view.findViewById(R.id.relativeSMS);
        txtCallIndicator=(TextView)view.findViewById(R.id.txtCallIndicator);
        txtSMSIndicator=(TextView)view.findViewById(R.id.txtSMSIndicator);
        txtCall=(TextView)view.findViewById(R.id.txtCall);
        txtSMS=(TextView)view.findViewById(R.id.txtSMS);
        pager=(ViewPager)view.findViewById(R.id.pager);

        relativeCall.setOnClickListener(this);
        relativeSMS.setOnClickListener(this);
        pager.addOnPageChangeListener(this);

        adapter=new BlockHistoryPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.relativeCall)
        {
            pager.setCurrentItem(0);
        }
        else if(view.getId()==R.id.relativeSMS)
        {
            pager.setCurrentItem(1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(position==0) {
            txtCallIndicator.setVisibility(View.VISIBLE);
            txtSMSIndicator.setVisibility(View.INVISIBLE);
            txtCall.setTextColor(getResources().getColor(R.color.colorPrimary));
            txtSMS.setTextColor(getResources().getColor(R.color.colorTextInactive));
        }
        else{
            txtCallIndicator.setVisibility(View.INVISIBLE);
            txtSMSIndicator.setVisibility(View.VISIBLE);
            txtSMS.setTextColor(getResources().getColor(R.color.colorPrimary));
            txtCall.setTextColor(getResources().getColor(R.color.colorTextInactive));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
