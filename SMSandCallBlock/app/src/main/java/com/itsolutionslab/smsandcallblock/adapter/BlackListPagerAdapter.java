package com.itsolutionslab.smsandcallblock.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.itsolutionslab.smsandcallblock.fragment.CallBlackListFragment;
import com.itsolutionslab.smsandcallblock.fragment.SMSBlackListFragment;

/**
 * Created by ASUS on 1/16/2016.
 */
public class BlackListPagerAdapter extends FragmentPagerAdapter {

    public BlackListPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0)
        {
            return new CallBlackListFragment();
        }
        else if(position==1)
        {
            return new SMSBlackListFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
