package com.itsolutionslab.smsandcallblock;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itsolutionslab.smsandcallblock.adapter.CallLogPagerAdapter;

/**
 * Created by ASUS on 1/17/2016.
 */
public class LogNumbersActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener{

    RelativeLayout relativeIncoming, relativeMissed;
    TextView txtIncomingIndicator, txtMissedIndicator, txtIncoming, txtMissed;

    ViewPager pager;
    CallLogPagerAdapter adapter;

    String fromOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);
        initView();
        setUiClickHandler();
        setAdapter();
    }

    private void initView() {
        Toolbar toolBar=(Toolbar)findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Call Log");

        relativeIncoming = (RelativeLayout) findViewById(R.id.relativeIncoming);
      //  relativeOutgoing = (RelativeLayout) findViewById(R.id.relativeOutgoing);
        relativeMissed = (RelativeLayout) findViewById(R.id.relativeMissed);
        txtIncoming = (TextView) findViewById(R.id.txtIncoming);
      //  txtOutgoing = (TextView) findViewById(R.id.txtOutgoing);
        txtMissed = (TextView) findViewById(R.id.txtMissed);
        txtIncomingIndicator = (TextView) findViewById(R.id.txtIncomingIndicator);
      //  txtOutgoingIndicator = (TextView) findViewById(R.id.txtOutgoingIndicator);
        txtMissedIndicator = (TextView) findViewById(R.id.txtMissedIndicator);
        pager=(ViewPager) findViewById(R.id.pager);

        fromOption = getIntent().getExtras().getString("from");
    }

    private void setUiClickHandler () {
        relativeIncoming.setOnClickListener(this);
     //   relativeOutgoing.setOnClickListener(this);
        relativeMissed.setOnClickListener(this);
        pager.addOnPageChangeListener(this);
    }

    private void setAdapter() {
        adapter=new CallLogPagerAdapter(getSupportFragmentManager(), fromOption);
        pager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.relativeIncoming)
        {
            pager.setCurrentItem(0);
        }
        else if(view.getId()==R.id.relativeMissed)
        {
            pager.setCurrentItem(1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(position==0) {
            txtIncomingIndicator.setVisibility(View.VISIBLE);
         //   txtOutgoingIndicator.setVisibility(View.INVISIBLE);
            txtMissedIndicator.setVisibility(View.INVISIBLE);
            txtIncoming.setTextColor(getResources().getColor(R.color.colorPrimary));
         //   txtOutgoing.setTextColor(getResources().getColor(R.color.colorTextInactive));
            txtMissed.setTextColor(getResources().getColor(R.color.colorTextInactive));
        }
        if(position==1) {
            txtIncomingIndicator.setVisibility(View.INVISIBLE);
           // txtOutgoingIndicator.setVisibility(View.INVISIBLE);
            txtMissedIndicator.setVisibility(View.VISIBLE);
            txtIncoming.setTextColor(getResources().getColor(R.color.colorTextInactive));
           // txtOutgoing.setTextColor(getResources().getColor(R.color.colorTextInactive));
            txtMissed.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
