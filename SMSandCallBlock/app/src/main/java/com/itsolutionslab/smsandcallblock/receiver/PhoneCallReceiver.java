package com.itsolutionslab.smsandcallblock.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.itsolutionslab.smsandcallblock.blockerservice.CallBlockerService;
import com.itsolutionslab.smsandcallblock.utility.SharedPrefUtil;

import java.lang.reflect.Method;

/**
 * Created by user on 3/1/2016.
 */
public class PhoneCallReceiver extends BroadcastReceiver {
    private static final String TAG = "Phone call";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "Receving....");
    /*    if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
        //    WakeLocker.acquire(context);
            String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            Intent service = new Intent(context, CallBlockerService.class);
            service.putExtra("number", number);
            context.startService(service);
        }   */
        SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(context);
        boolean callBlockStatus = sharedPrefUtil.getCallBlockStatus();
        if(callBlockStatus) {
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            PhoneCallStateListener customPhoneListener = new PhoneCallStateListener(context);
            telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }


}
