package com.itsolutionslab.smsandcallblock.db;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.itsolutionslab.smsandcallblock.utility.Constants;

public class DataHandler extends SQLiteOpenHelper{
	
	
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "call_sms_block";
	public static final String TABLE_CALL_BLACK_LIST = "call_black_list";
	public static final String TABLE_SMS_BLACK_LIST = "sms_black_list";
	public static final String TABLE_CALL_BLOCK_LIST = "call_block_list";
	public static final String TABLE_SMS_BLOCK_LIST = "sms_block_list";
	public static final String BLACK_LIST_ID = "id";
	public static final String BLACK_LIST_NAME = "name";
	public static final String BLACK_LIST_NUMBER = "number";
	public static final String BLACK_LIST_PHOTO = "photo";
	public static final String BLOCK_LIST_ID = "id";
	public static final String BLOCK_LIST_NAME = "name";
	public static final String BLOCK_LIST_NUMBER = "number";
	public static final String BLOCK_LIST_PHOTO = "photo";
	public static final String BLOCK_LIST_DATE = "date";
	public static final String BLOCK_LIST_TIME = "time";
	

	public DataHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_CALL_BLACK_LIST_TABLE = "CREATE TABLE " + TABLE_CALL_BLACK_LIST + "("+ BLACK_LIST_ID +
				" INTEGER PRIMARY KEY AUTOINCREMENT, " + BLACK_LIST_NAME + " TEXT, "+ BLACK_LIST_NUMBER + " TEXT, "+ BLACK_LIST_PHOTO + " TEXT" + ");";
		String CREATE_SMS_BLACK_LIST_TABLE = "CREATE TABLE " + TABLE_SMS_BLACK_LIST + "("+ BLACK_LIST_ID +
				" INTEGER PRIMARY KEY AUTOINCREMENT, " + BLACK_LIST_NAME + " TEXT, "+ BLACK_LIST_NUMBER + " TEXT, "+ BLACK_LIST_PHOTO + " TEXT" + ");";
		String CREATE_CALL_BLOCK_LIST_TABLE = "CREATE TABLE " + TABLE_CALL_BLOCK_LIST + "("+ BLOCK_LIST_ID +
				" INTEGER PRIMARY KEY AUTOINCREMENT, " + BLOCK_LIST_NAME + " TEXT, "+ BLOCK_LIST_NUMBER + " TEXT, "+ BLOCK_LIST_PHOTO + " TEXT, "  + BLOCK_LIST_DATE + " TEXT, " + BLOCK_LIST_TIME + " TEXT" + ");";
		String CREATE_SMS_BLOCK_LIST_TABLE = "CREATE TABLE " + TABLE_SMS_BLOCK_LIST + "("+ BLOCK_LIST_ID +
				" INTEGER PRIMARY KEY AUTOINCREMENT, " + BLOCK_LIST_NAME + " TEXT, "+ BLOCK_LIST_NUMBER + " TEXT, "+ BLOCK_LIST_PHOTO + " TEXT, "  + BLOCK_LIST_DATE + " TEXT, " + BLOCK_LIST_TIME + " TEXT" + ");";
        db.execSQL(CREATE_CALL_BLACK_LIST_TABLE);
		db.execSQL(CREATE_SMS_BLACK_LIST_TABLE);
		db.execSQL(CREATE_CALL_BLOCK_LIST_TABLE);
		db.execSQL(CREATE_SMS_BLOCK_LIST_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALL_BLACK_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SMS_BLACK_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALL_BLOCK_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SMS_BLOCK_LIST);
        // Create tables again
        onCreate(db);
	}
	
	public void addCallBlackList(String name, String phone, String photo)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		 
        ContentValues values = new ContentValues();
        values.put(BLACK_LIST_NAME, name);
        values.put(BLACK_LIST_NUMBER, phone);
		values.put(BLACK_LIST_PHOTO, photo);
        db.insert(TABLE_CALL_BLACK_LIST, null, values);
        db.close();
	}

	public void addSmsBlackList(String name, String phone, String photo)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BLACK_LIST_NAME, name);
		values.put(BLACK_LIST_NUMBER, phone);
		values.put(BLACK_LIST_PHOTO, photo);
		db.insert(TABLE_SMS_BLACK_LIST, null, values);
		db.close();
	}

	public void addCallBlockList(String name, String phone, String photo, String date, String time)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BLOCK_LIST_NAME, name);
		values.put(BLOCK_LIST_NUMBER, phone);
		values.put(BLOCK_LIST_PHOTO, photo);
		values.put(BLOCK_LIST_DATE, date);
		values.put(BLOCK_LIST_TIME, time);
		db.insert(TABLE_CALL_BLOCK_LIST, null, values);
		db.close();
	}

	public void addSmsBlockList(String name, String phone, String photo, String date, String time)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BLOCK_LIST_NAME, name);
		values.put(BLOCK_LIST_NUMBER, phone);
		values.put(BLOCK_LIST_PHOTO, photo);
		values.put(BLOCK_LIST_DATE, date);
		values.put(BLOCK_LIST_TIME, time);
		db.insert(TABLE_SMS_BLOCK_LIST, null, values);
		db.close();
	}
	
	public void deleteCallBlackList(String number)
	{
		String[] selections={number};
		SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CALL_BLACK_LIST, BLACK_LIST_NUMBER + " = ?", selections);
        db.close();
	}

	public void deleteSmsBlackList(String number)
	{
		String[] selections={number};
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SMS_BLACK_LIST, BLACK_LIST_NUMBER + " = ?",selections);
		db.close();
	}

	public void deleteCallBlockList(String number)
	{
		String[] selections={number};
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CALL_BLOCK_LIST, BLOCK_LIST_NUMBER + " = ?", selections);
		db.close();
	}

	public void deleteSmsBlockList(String number)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String[] selections={number};
		db.delete(TABLE_SMS_BLOCK_LIST, BLOCK_LIST_NUMBER + " = ?", selections);
	//	String deleteQuery = "DELETE FROM " + TABLE_SMS_BLOCK_LIST + " WHERE " + BLOCK_LIST_NUMBER + " LIKE " + "'" + number + "'";
	//	db.execSQL(deleteQuery);
		db.close();
	}

	/*
	int getContactCount()
	{
		String countQuery = "SELECT  * FROM " + TABLE_CALL_BLACK_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count=cursor.getCount();
        db.close();
		return count;
	}  */
	
	/*
    Cursor getContact(int id)
    {
    	SQLiteDatabase db = this.getReadableDatabase();
    	String[] selections={String.valueOf(id)};
    	String columns[]={BLACK_LIST_NAME,BLACK_LIST_NUMBER};
    	Cursor cursor=db.query(TABLE_CALL_BLACK_LIST, columns, BLACK_LIST_ID + "=?", selections, null, null, null);
    	//db.close();
    	return cursor;
    }   */

	public HashMap<String, ArrayList<String>> getAllCallBlackList()
    {
		HashMap<String, ArrayList<String>> blackList = new HashMap<>();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<String> numbers = new ArrayList<>();
		ArrayList<String> photos = new ArrayList<>();
    	String selectQuery = "SELECT  * FROM " + TABLE_CALL_BLACK_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do
			{
				String name=cursor.getString(cursor.getColumnIndex(BLACK_LIST_NAME));
				String number=cursor.getString(cursor.getColumnIndex(BLACK_LIST_NUMBER));
				String photo=cursor.getString(cursor.getColumnIndex(BLACK_LIST_PHOTO));
				names.add(name);
				numbers.add(number);
				photos.add(photo);
			}
			while(cursor.moveToNext());
			blackList.put(Constants.NAME, names);
			blackList.put(Constants.NUMBER, numbers);
			blackList.put(Constants.PHOTO, photos);
			db.close();
			return blackList;
		}
        db.close();
    	return null;
    }

	public HashMap<String, ArrayList<String>> getAllSmsBlackList()
	{
		HashMap<String, ArrayList<String>> blackList = new HashMap<>();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<String> numbers = new ArrayList<>();
		ArrayList<String> photos = new ArrayList<>();
		String selectQuery = "SELECT  * FROM " + TABLE_SMS_BLACK_LIST;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do
			{
				String name=cursor.getString(cursor.getColumnIndex(BLACK_LIST_NAME));
				String number=cursor.getString(cursor.getColumnIndex(BLACK_LIST_NUMBER));
				String photo=cursor.getString(cursor.getColumnIndex(BLACK_LIST_PHOTO));
				names.add(name);
				numbers.add(number);
				photos.add(photo);
			}
			while(cursor.moveToNext());
			blackList.put(Constants.NAME, names);
			blackList.put(Constants.NUMBER, numbers);
			blackList.put(Constants.PHOTO, photos);
			db.close();
			return blackList;
		}
		db.close();
		return null;
	}

	public HashMap<String, ArrayList<String>> getAllCallBlockList()
	{
		HashMap<String, ArrayList<String>> blockList = new HashMap<>();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<String> numbers = new ArrayList<>();
		ArrayList<String> photos = new ArrayList<>();
		ArrayList<String> dates = new ArrayList<>();
		ArrayList<String> times = new ArrayList<>();
		String selectQuery = "SELECT  * FROM " + TABLE_CALL_BLOCK_LIST;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do
			{
				String name=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_NAME));
				String number=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_NUMBER));
				String photo=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_PHOTO));
				String date=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_DATE));
				String time=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_TIME));
				names.add(name);
				numbers.add(number);
				photos.add(photo);
				dates.add(date);
				times.add(time);
			}
			while(cursor.moveToNext());
			blockList.put(Constants.NAME, names);
			blockList.put(Constants.NUMBER, numbers);
			blockList.put(Constants.PHOTO, photos);
			blockList.put(Constants.DATE, dates);
			blockList.put(Constants.TIME, times);
			db.close();
			return blockList;
		}
		db.close();
		return null;
	}

	public HashMap<String, ArrayList<String>> getAllSmsBlockList()
	{
		HashMap<String, ArrayList<String>> blockList = new HashMap<>();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<String> numbers = new ArrayList<>();
		ArrayList<String> photos = new ArrayList<>();
		ArrayList<String> dates = new ArrayList<>();
		ArrayList<String> times = new ArrayList<>();
		String selectQuery = "SELECT  * FROM " + TABLE_SMS_BLOCK_LIST;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do
			{
				String name=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_NAME));
				String number=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_NUMBER));
				String photo=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_PHOTO));
				String date=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_DATE));
				String time=cursor.getString(cursor.getColumnIndex(BLOCK_LIST_TIME));
				names.add(name);
				numbers.add(number);
				photos.add(photo);
				dates.add(date);
				times.add(time);
			}
			while(cursor.moveToNext());
			blockList.put(Constants.NAME, names);
			blockList.put(Constants.NUMBER, numbers);
			blockList.put(Constants.PHOTO, photos);
			blockList.put(Constants.DATE, dates);
			blockList.put(Constants.TIME, times);
			db.close();
			return blockList;
		}
		db.close();
		return null;
	}

	/*
    public int updateContact(int id,String name,String phone)
    {
    	SQLiteDatabase db=this.getWritableDatabase();
    	ContentValues values = new ContentValues();
        values.put(BLACK_LIST_NAME, name);
        values.put(BLACK_LIST_NUMBER, phone);
        
        String[] selections={String.valueOf(id)};
        return db.update(TABLE_CALL_BLACK_LIST, values, BLACK_LIST_ID + "=?", selections);
    }   */
	

}
