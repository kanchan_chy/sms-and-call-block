package com.itsolutionslab.smsandcallblock;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;

import com.itsolutionslab.smsandcallblock.utility.AppData;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by ASUS on 1/15/2016.
 */
public class SplashActivity extends AppCompatActivity {

   // Bitmap defaultBitmap;
   // String nameText="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
     //   defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
        new TimeSpender().execute();
    }


    @Override
    public void onBackPressed() {

    }

    public void getContactListContacts(ContentResolver cr)
    {
        ArrayList<String> numbers= new ArrayList<String>();
        ArrayList<String> names= new ArrayList<String>();
        ArrayList<Bitmap> photos= new ArrayList<Bitmap>();

        try {
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            if(phones != null) {
                if(phones.moveToFirst()) {
                    do {
                        Long id = phones.getLong(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
                        String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),
                                ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id)));
                        Bitmap bitmap;
                        if (inputStream != null) bitmap = BitmapFactory.decodeStream(inputStream);
                        else bitmap = null;
                        numbers.add(phoneNumber);
                        names.add(name);
                        photos.add(bitmap);
                    }
                    while (phones.moveToNext());
                }
                phones.close();
            }
        } catch (Exception e2) {

        }

        if(numbers == null ) {
            numbers = new ArrayList<>();
        }
        if(names == null ) {
            names = new ArrayList<>();
        }
        if(photos == null ) {
            photos = new ArrayList<>();
        }

        AppData.setContactListNames(names);
        AppData.setContactListNumbers(numbers);
        AppData.setContactListPhotos(photos);
    }

    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    private void getMessageContacts()
    {
        ArrayList<String> numbers= new ArrayList<String>();
        ArrayList<String> names= new ArrayList<String>();

        try {
            Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
            if(cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        String phNumber=cursor.getString(cursor.getColumnIndex("address"));
                        if(!numbers.contains(phNumber))
                        {
                            String phName = getContactName(SplashActivity.this, phNumber);
                            if(phName == null) phName ="";
                            numbers.add(phNumber);
                            names.add(phName);
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception e2) {

        }

        if(numbers == null ) {
            numbers = new ArrayList<>();
        }
        if(names == null ) {
            names = new ArrayList<>();
        }

        AppData.setInboxNumbers(numbers);
        AppData.setInboxNames(names);
    }

    private void getLogContacts()
    {
        ArrayList<String> incomingNumbers= new ArrayList<String>();
        ArrayList<String> outgoingNumbers= new ArrayList<String>();
        ArrayList<String> missedNumbers= new ArrayList<String>();
        ArrayList<String> incomingNames= new ArrayList<String>();
        ArrayList<String> outgoingNames= new ArrayList<String>();
        ArrayList<String> missedNames= new ArrayList<String>();

        try {
            String strOrder = android.provider.CallLog.Calls.DATE + " DESC";
  /* Query the CallLog Content Provider */
            Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, strOrder);
            if(managedCursor != null) {
                int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
                //  int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                //  int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
                if(managedCursor.moveToFirst()) {
                    do {
                        String phNum = managedCursor.getString(number);
                        String callTypeCode = managedCursor.getString(type);
                        String phName = managedCursor.getString(name);
                        if(phName == null) phName = "";
                        //  String strcallDate = managedCursor.getString(date);
                        //  Date callDate = new Date(Long.valueOf(strcallDate));
                        //  String callDuration = managedCursor.getString(duration);
                        int callcode = Integer.parseInt(callTypeCode);
                        switch (callcode) {
                            case CallLog.Calls.OUTGOING_TYPE:
                                if(!outgoingNumbers.contains(phNum)) {
                                    outgoingNumbers.add(phNum);
                                    outgoingNames.add(phName);
                                }
                                break;
                            case CallLog.Calls.INCOMING_TYPE:
                                if(!incomingNumbers.contains(phNum)) {
                                    incomingNumbers.add(phNum);
                                    incomingNames.add(phName);
                                }
                                break;
                            case CallLog.Calls.MISSED_TYPE:
                                if(!missedNumbers.contains(phNum)) {
                                    missedNumbers.add(phNum);
                                    missedNames.add(phName);
                                }
                                break;
                        }
                    } while (managedCursor.moveToNext());
                }
                managedCursor.close();
            }
        } catch (Exception e2) {

        }

        if(incomingNumbers == null ) {
            incomingNumbers = new ArrayList<>();
        }
        if(outgoingNumbers == null ) {
            outgoingNumbers = new ArrayList<>();
        }
        if(missedNumbers == null ) {
            missedNumbers = new ArrayList<>();
        }
        if(outgoingNames == null ) {
            outgoingNames = new ArrayList<>();
        }
        if(incomingNames == null ) {
            incomingNames = new ArrayList<>();
        }
        if(missedNames == null ) {
            missedNames = new ArrayList<>();
        }

        AppData.setIncomingNumbers(incomingNumbers);
        AppData.setOutgoingNumbers(outgoingNumbers);
        AppData.setMissedNumbers(missedNumbers);
        AppData.setOutgoingNames(outgoingNames);
        AppData.setIncomingNames(incomingNames);
        AppData.setMissedNames(missedNames);
    }

    class TimeSpender extends AsyncTask<String,String,String>{

        boolean isOk = true;

        @Override
        protected String doInBackground(String... strings) {
            try {
                getContactListContacts(SplashActivity.this.getContentResolver());
                getMessageContacts();
                getLogContacts();
            }
            catch (Exception e){
                isOk = false;
                try {
                    Thread.sleep(500);
                }
                catch (Exception e1) {
                }
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            isOk = true;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }

    }


}
