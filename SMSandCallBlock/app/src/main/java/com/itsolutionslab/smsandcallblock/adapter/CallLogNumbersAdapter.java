package com.itsolutionslab.smsandcallblock.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.AppData;
import com.itsolutionslab.smsandcallblock.utility.Constants;
import com.itsolutionslab.smsandcallblock.widget.CustomToast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 10/18/2015.
 */
public class CallLogNumbersAdapter extends RecyclerView.Adapter<CallLogNumbersAdapter.CallLogNumbersViewHolder>{

    public static Activity mContext;
    String fromOption;
    ArrayList<String>numbers=new ArrayList<>();
    ArrayList<String>names=new ArrayList<>();
    ArrayList<String>savedNames=new ArrayList<>();
    ArrayList<Bitmap>savedPhotos=new ArrayList<>();
    String callType;

    ArrayList<String>blackListNumbers = new ArrayList<>();
    ArrayList<Boolean>isAdded = new ArrayList<>();
    DataHandler dataHandler;

    Bitmap defaultBitmap;

    public CallLogNumbersAdapter(Activity mContext, String callType, String fromOption)
    {
        this.mContext = mContext;
        this.callType = callType;
        this.fromOption = fromOption;
        savedNames = AppData.getContactListNames();
        savedPhotos = AppData.getContactListPhotos();
        if(callType.equals(Constants.CALL_INCOMING)) {
            numbers = AppData.getIncomingNumbers();
            names = AppData.getIncomingNames();
        }
        if(callType.equals(Constants.CALL_OUTGOING)) {
            numbers = AppData.getOutgoingNumbers();
            names = AppData.getOutgoingNames();
        }
        if(callType.equals(Constants.CALL_MISSED)) {
            numbers = AppData.getMissedNumbers();
            names = AppData.getMissedNames();
        }

        isAdded = new ArrayList<>();
        for (int i=0; i<numbers.size(); i++) {
            isAdded.add(false);
        }
        try {
            defaultBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.user);
            dataHandler = new DataHandler(mContext);
            HashMap<String, ArrayList<String>> hashBlackList;
            if(fromOption.equals(Constants.OPTION_CALL)) {
                hashBlackList = dataHandler.getAllCallBlackList();
            }
            else {
                hashBlackList = dataHandler.getAllSmsBlackList();
            }
            if(hashBlackList != null) {
                blackListNumbers = hashBlackList.get(Constants.NUMBER);
            }
        }
        catch (Exception e) {

        }
    }

    @Override
    public CallLogNumbersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater=LayoutInflater.from(mContext);
        return new CallLogNumbersViewHolder(mInflater.inflate(R.layout.row_contact_list, parent, false));
    }

    @Override
    public void onBindViewHolder(CallLogNumbersViewHolder holder, int pos) {
        String tempName = names.get(pos);
        if(tempName.equals("")) {
            holder.txtTitle.setText(numbers.get(pos));
            holder.txtExtra.setText("Unsaved");
            holder.imgContact.setImageBitmap(defaultBitmap);
        }
        else {
            holder.txtTitle.setText(names.get(pos));
            holder.txtExtra.setText(numbers.get(pos));
            int tempPosition = savedNames.indexOf(tempName);
            if(savedPhotos.get(tempPosition) != null) holder.imgContact.setImageBitmap(savedPhotos.get(tempPosition));
            else holder.imgContact.setImageBitmap(defaultBitmap);
        }

        String fullNumber = numbers.get(pos);
        if(blackListNumbers.contains(fullNumber)) {
            isAdded.set(pos, true);
            holder.imgBtnAdd.setImageResource(R.drawable.close_circular);
        }
        else {
            isAdded.set(pos, false);
            holder.imgBtnAdd.setImageResource(R.drawable.add_circular);
        }
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public class CallLogNumbersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView imgContact;
        public TextView txtTitle,txtExtra;
        public ImageButton imgBtnAdd;

        public CallLogNumbersViewHolder(View view)
        {
            super(view);
            imgContact=(ImageView)view.findViewById(R.id.imgContact);
            txtTitle=(TextView)view.findViewById(R.id.txtTitle);
            txtExtra=(TextView)view.findViewById(R.id.txtExtra);
            imgBtnAdd = (ImageButton) view.findViewById(R.id.imgBtnAdd);

            imgBtnAdd.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            addOrRemoveNumber(pos, imgBtnAdd);
        }
    }


    private void addOrRemoveNumber(int pos, ImageButton imgBtnAdd) {
        if(isAdded.get(pos)) {
            if(fromOption.equals(Constants.OPTION_CALL)) {
                dataHandler.deleteCallBlackList(numbers.get(pos));
            }
            else {
                dataHandler.deleteSmsBlackList(numbers.get(pos));
            }
            String fullNumber = numbers.get(pos);
            blackListNumbers.remove(fullNumber);
            isAdded.set(pos, false);
            imgBtnAdd.setImageResource(R.drawable.add_circular);
            String value = names.get(pos);
            if(value.equals("")) value = numbers.get(pos);
            new CustomToast(mContext, value + " is removed from blacklist");
        }
        else {
            String tempPhoto = "";
            if(names.get(pos).equals("")) {
                tempPhoto = "";
            }
            else {
                int tempPosition = savedNames.indexOf(names.get(pos));
                if(savedPhotos.get(tempPosition) == null) {
                    tempPhoto = "";
                }
                else {
                    tempPhoto = BitMapToString(savedPhotos.get(tempPosition));
                }
            }
            if(fromOption.equals(Constants.OPTION_CALL)) {
                dataHandler.addCallBlackList(names.get(pos), numbers.get(pos), tempPhoto);
            }
            else {
                dataHandler.addSmsBlackList(names.get(pos), numbers.get(pos), tempPhoto);
            }
            String fullNumber = numbers.get(pos);
            blackListNumbers.add(fullNumber);
            isAdded.set(pos, true);
            imgBtnAdd.setImageResource(R.drawable.close_circular);
            String value = names.get(pos);
            if(value.equals("")) value = numbers.get(pos);
            new CustomToast(mContext, value + " is added in blacklist");
        }
    }


    private void showAlertDialog(final int pos, final ImageButton imgBtnAdd) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Alert!");
        String okButton = "";
        if(isAdded.get(pos)) {
            okButton = "Remove";
            builder.setMessage("This number is in your blacklist. Do you want to remove it from blacklist?");
        }
        else {
            okButton = "Add";
            builder.setMessage("This number is not in your blacklist. Do you want to add it in blacklist?");
        }

        builder.setCancelable(true);

        builder.setPositiveButton(okButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addOrRemoveNumber(pos, imgBtnAdd);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public String BitMapToString(Bitmap bitmap){
        try {
            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b=baos.toByteArray();
            String temp= Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        }
        catch (Exception e1) {
            Toast.makeText(mContext, "" + e1, Toast.LENGTH_SHORT).show();
            return "";
        }
    }



}
