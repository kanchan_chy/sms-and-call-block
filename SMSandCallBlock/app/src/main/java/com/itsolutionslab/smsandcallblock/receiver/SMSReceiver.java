package com.itsolutionslab.smsandcallblock.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.blockerservice.CallBlockerService;
import com.itsolutionslab.smsandcallblock.blockerservice.SMSBlockerService;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.Constants;
import com.itsolutionslab.smsandcallblock.utility.SharedPrefUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

/**
 * Created by user on 3/1/2016.
 */
public class SMSReceiver extends BroadcastReceiver {
    private static final String TAG = "SMS";

    ArrayList<String>blackListNumbers = new ArrayList<>();
    ArrayList<String>blackListNames = new ArrayList<>();
    ArrayList<String>blackListPhotos = new ArrayList<>();

    String number = "", name = "", photo = "", date = "", time = "";
    String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    DataHandler dataHandler;
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(context);
            boolean smsBlockStatus = sharedPrefUtil.getSmsBlockStatus();
            if(smsBlockStatus){
                Bundle bundle=intent.getExtras();
                if(bundle!=null)
                {
                    SmsMessage[] allMessages=null;
                    number = "";
                    Object[] objects=(Object[])bundle.get("pdus");
                    allMessages=new SmsMessage[objects.length];
                    for(int i=0;i<objects.length;i++)
                    {
                        allMessages[i]=SmsMessage.createFromPdu((byte[])objects[i]);
                        number=allMessages[i].getOriginatingAddress();
                        if(!number.equals("")){
                            Log.e("Incoming number", number);
                            break;
                        }
                    }
                    if(!number.equals("")) {
                        try {

                            dataHandler = new DataHandler(context);
                            HashMap<String, ArrayList<String>> hashBlackList = dataHandler.getAllSmsBlackList();
                            if(hashBlackList != null) {
                                blackListNumbers = hashBlackList.get(Constants.NUMBER);
                                blackListNames = hashBlackList.get(Constants.NAME);
                                blackListPhotos = hashBlackList.get(Constants.PHOTO);

                                boolean flag = false;
                                for (int i=0; i<blackListNumbers.size(); i++) {
                                    if(blackListNumbers.get(i).equals(number)) {
                                        name = blackListNames.get(i);
                                        photo = blackListPhotos.get(i);
                                        flag = true;
                                        break;
                                    }
                                }

                                if(flag) {
                                    Log.e("aborting_called", "true");
                                    //   WakeLocker.acquire(context);
                               /*     Intent service = new Intent(context, SMSBlockerService.class);
                                    service.putExtra("number", number);
                                    service.putExtra("name", name);
                                    service.putExtra("photo", photo);
                                    context.startService(service);  */
                                    // loadIntoDatabase();
                                    //  sendNotification();
                                    abortBroadcast();
                                }
                            }
                        }
                        catch (Exception e1) {

                        }
                    }
                }
            }

        }

    }




    private void loadIntoDatabase() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int status = calendar.get(Calendar.AM_PM);
        String am_Pm_status;
        if(status == 0) am_Pm_status = "AM";
        else am_Pm_status = "PM";
        date = months[month] + " " + day + ", " + year;
        time = hour + ":" + minute + " " + am_Pm_status;
        dataHandler.addSmsBlockList(name, number, photo, date, time);
    }

    private void sendNotification() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.icon);

        String value;
        if(!name.equals("")) value = name;
        else value = number;
        String msg = "SMS blocked from " + value + " at " +date + " " + time +".";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setLargeIcon(notificationLargeIconBitmap)
                .setContentTitle("SMS and Call Blocker")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setAutoCancel(true);

        // mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(new Random().nextInt(), mBuilder.build());
    }




}
