package com.itsolutionslab.smsandcallblock.utility;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by ASUS on 2/4/2016.
 */
public class AppData {

    public static ArrayList<String> contactListNames;
    public static ArrayList<String>contactListNumbers;
    public static ArrayList<Bitmap>contactListPhotos;
    public static ArrayList<String>inboxNumbers;
    public static ArrayList<String>inboxNames;
    public static ArrayList<String>incomingNumbers;
    public static ArrayList<String>incomingNames;
    public static ArrayList<String>outgoingNumbers;
    public static ArrayList<String>outgoingNames;
    public static ArrayList<String>missedNumbers;
    public static ArrayList<String>missedNames;

    public static ArrayList<String> getInboxNames() {
        return inboxNames;
    }

    public static void setInboxNames(ArrayList<String> inboxNames) {
        AppData.inboxNames = inboxNames;
    }

    public static ArrayList<String> getIncomingNames() {
        return incomingNames;
    }

    public static void setIncomingNames(ArrayList<String> incomingNames) {
        AppData.incomingNames = incomingNames;
    }

    public static ArrayList<String> getOutgoingNames() {
        return outgoingNames;
    }

    public static void setOutgoingNames(ArrayList<String> outgoingNames) {
        AppData.outgoingNames = outgoingNames;
    }

    public static ArrayList<String> getMissedNames() {
        return missedNames;
    }

    public static void setMissedNames(ArrayList<String> missedNames) {
        AppData.missedNames = missedNames;
    }

    public static ArrayList<String> getInboxNumbers() {
        return inboxNumbers;
    }

    public static void setInboxNumbers(ArrayList<String> inboxNumbers) {
        AppData.inboxNumbers = inboxNumbers;
    }

    public static ArrayList<String> getIncomingNumbers() {
        return incomingNumbers;
    }

    public static void setIncomingNumbers(ArrayList<String> incomingNumbers) {
        AppData.incomingNumbers = incomingNumbers;
    }

    public static ArrayList<String> getOutgoingNumbers() {
        return outgoingNumbers;
    }

    public static void setOutgoingNumbers(ArrayList<String> outgoingNumbers) {
        AppData.outgoingNumbers = outgoingNumbers;
    }

    public static ArrayList<String> getMissedNumbers() {
        return missedNumbers;
    }

    public static void setMissedNumbers(ArrayList<String> missedNumbers) {
        AppData.missedNumbers = missedNumbers;
    }

    public static ArrayList<String> getContactListNames() {
        return contactListNames;
    }

    public static void setContactListNames(ArrayList<String> contactListNames) {
        AppData.contactListNames = contactListNames;
    }

    public static ArrayList<String> getContactListNumbers() {
        return contactListNumbers;
    }

    public static void setContactListNumbers(ArrayList<String> contactListNumbers) {
        AppData.contactListNumbers = contactListNumbers;
    }

    public static ArrayList<Bitmap> getContactListPhotos() {
        return contactListPhotos;
    }

    public static void setContactListPhotos(ArrayList<Bitmap> contactListPhotos) {
        AppData.contactListPhotos = contactListPhotos;
    }

}
