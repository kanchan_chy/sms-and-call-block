package com.itsolutionslab.smsandcallblock.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.itsolutionslab.smsandcallblock.AddBlackListActivity;
import com.itsolutionslab.smsandcallblock.InboxNumbersActivity;
import com.itsolutionslab.smsandcallblock.LogNumbersActivity;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.adapter.BlackListAdapter;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 1/15/2016.
 */
public class SMSBlackListFragment extends Fragment implements View.OnClickListener{

    static TextView txtEmpty;
    RecyclerView recyclerBlackList;
    FloatingActionButton fab;

    BlackListAdapter adapter;

    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;

    DataHandler dataHandler;

    ArrayList<String> names=new ArrayList<>();
    ArrayList<String>numbers=new ArrayList<>();
    ArrayList<String>photos=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sms_black_list,container,false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtEmpty=(TextView)view.findViewById(R.id.txtEmpty);
        recyclerBlackList=(RecyclerView)view.findViewById(R.id.recyclerBlackList);
        fab=(FloatingActionButton)view.findViewById(R.id.fab);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerBlackList.setLayoutManager(layoutManager);

        fab.setOnClickListener(this);

        dataHandler = new DataHandler(getActivity());

        recyclerBlackList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                    onHide();
                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                    onShow();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }

                if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
                    scrolledDistance += dy;
                }

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        prepareListItems();
        if(numbers.size()>0){
            txtEmpty.setVisibility(View.GONE);
            adapter=new BlackListAdapter(getActivity(), names, numbers, photos, Constants.OPTION_SMS);
            recyclerBlackList.setAdapter(adapter);
        }
        else {
            showEmptyView();
        }
    }


    public static void showEmptyView() {
        txtEmpty.setVisibility(View.VISIBLE);
    }

    private void onHide(){
        fab.setVisibility(View.GONE);
    }

    private void onShow(){
        fab.setVisibility(View.VISIBLE);
    }


    private void prepareListItems()
    {
        names=new ArrayList<>();
        numbers=new ArrayList<>();
        photos = new ArrayList<>();
        try {
            HashMap<String, ArrayList<String>> hashBlackList = dataHandler.getAllSmsBlackList();
            if(hashBlackList != null) {
                names = hashBlackList.get(Constants.NAME);
                numbers = hashBlackList.get(Constants.NUMBER);
                photos = hashBlackList.get(Constants.PHOTO);
            }
        }
        catch (Exception e1) {

        }
    }


    private void showOptionListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_list, null);
        builder.setView(view);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();

        TextView txtAlertTitle = (TextView)view.findViewById(R.id.txtAlertTitle);
        TextView txtAlertLog = (TextView)view.findViewById(R.id.txtLog);
        TextView txtAlertSms = (TextView)view.findViewById(R.id.txtSMS);
        ImageButton imgBtnClose = (ImageButton)view.findViewById(R.id.imgBtnClose);

        txtAlertTitle.setText("Add contact for blocking SMS");

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtAlertLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent in = new Intent(getActivity(), LogNumbersActivity.class);
                        in.putExtra("from", Constants.OPTION_SMS);
                        startActivity(in);
                        alertDialog.dismiss();
                    }
                }, 100);
            }
        });

        txtAlertSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent in = new Intent(getActivity(), InboxNumbersActivity.class);
                        in.putExtra("from", Constants.OPTION_SMS);
                        startActivity(in);
                        alertDialog.dismiss();
                    }
                }, 100);
            }
        });

        alertDialog.show();

    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.fab)
        {
            showOptionListDialog();
          /*  Intent in = new Intent(getActivity(), AddBlackListActivity.class);
            in.putExtra("from", Constants.OPTION_SMS);
            startActivity(in);  */
        }
    }

}
