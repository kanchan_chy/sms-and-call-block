package com.itsolutionslab.smsandcallblock.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.itsolutionslab.smsandcallblock.fragment.CallBlockHistoryFragment;
import com.itsolutionslab.smsandcallblock.fragment.SMSBlockHistoryFragment;

/**
 * Created by ASUS on 1/16/2016.
 */
public class BlockHistoryPagerAdapter extends FragmentPagerAdapter {

    public BlockHistoryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0)
        {
            return new CallBlockHistoryFragment();
        }
        else if(position==1)
        {
            return new SMSBlockHistoryFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
