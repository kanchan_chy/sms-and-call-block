package com.itsolutionslab.smsandcallblock.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.fragment.CallBlackListFragment;
import com.itsolutionslab.smsandcallblock.fragment.CallBlockHistoryFragment;
import com.itsolutionslab.smsandcallblock.fragment.SMSBlackListFragment;
import com.itsolutionslab.smsandcallblock.fragment.SMSBlockHistoryFragment;
import com.itsolutionslab.smsandcallblock.utility.Constants;

import java.util.ArrayList;

/**
 * Created by ASUS on 10/18/2015.
 */
public class BlockListAdapter extends RecyclerView.Adapter<BlockListAdapter.BlockListViewHolder>{

    public static Activity mContext;
    ArrayList<String>names=new ArrayList<>();
    ArrayList<String>numbers=new ArrayList<>();
    ArrayList<String>photos=new ArrayList<>();
    ArrayList<String>dates=new ArrayList<>();
    ArrayList<String>times=new ArrayList<>();
    String blockType;

    Bitmap defaultBitmap;

    public BlockListAdapter(Activity mContext, ArrayList<String> names, ArrayList<String> numbers, ArrayList<String> photos, ArrayList<String>dates, ArrayList<String>times, String blockType)
    {
        this.mContext = mContext;
        this.names=names;
        this.numbers=numbers;
        this.photos = photos;
        this.dates = dates;
        this.times = times;
        this.blockType=blockType;
        defaultBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.user);
    }

    @Override
    public BlockListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater=LayoutInflater.from(mContext);
        return new BlockListViewHolder(mInflater.inflate(R.layout.row_block_list, parent, false));
    }

    @Override
    public void onBindViewHolder(BlockListViewHolder holder, int pos) {
        if(!names.get(pos).equals("")) {
            holder.txtTitle.setText(names.get(pos));
            holder.txtExtra.setText(numbers.get(pos));
        }
        else {
            holder.txtTitle.setText(numbers.get(pos));
            holder.txtExtra.setText("Unsaved");
        }
        String tempPhoto = photos.get(pos);
        if(!tempPhoto.equals("")) {
            Bitmap photo = StringToBitMap(tempPhoto);
            holder.imgContact.setImageBitmap(photo);
        }
        else {
            holder.imgContact.setImageBitmap(defaultBitmap);
        }
        holder.txtDate.setText(dates.get(pos));
        holder.txtTime.setText(times.get(pos));
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public class BlockListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView imgContact;
        public ImageButton imgBtnClose;
        public TextView txtTitle,txtExtra, txtDate, txtTime;

        public BlockListViewHolder(View view)
        {
            super(view);
            imgContact=(ImageView)view.findViewById(R.id.imgContact);
            imgBtnClose=(ImageButton)view.findViewById(R.id.imgBtnClose);
            txtTitle=(TextView)view.findViewById(R.id.txtTitle);
            txtExtra=(TextView)view.findViewById(R.id.txtExtra);
            txtDate=(TextView)view.findViewById(R.id.txtDate);
            txtTime=(TextView)view.findViewById(R.id.txtTime);
            imgBtnClose.setOnClickListener(this);
        }

        public void onClick(View view)
        {
            final int pos=getAdapterPosition();
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int position = getAdapterPosition();
                    showAlertDialog(position);
                }

            },100);
        }

    }


    private void deleteNumber(int position) {
        String tempNumber = numbers.get(position);
        try {
            DataHandler dataHandler = new DataHandler(mContext);
            if(blockType.equals(Constants.OPTION_CALL)) {
                dataHandler.deleteCallBlockList(tempNumber);
            }
            else {
                dataHandler.deleteSmsBlockList(tempNumber);
            }
            names.remove(position);
            numbers.remove(position);
            photos.remove(position);
            dates.remove(position);
            times.remove(position);
            notifyItemRemoved(position);
            if(numbers.size() == 0) {
                if(blockType.equals(Constants.OPTION_CALL)) {
                    CallBlockHistoryFragment.showEmptyView();
                }
                else {
                    SMSBlockHistoryFragment.showEmptyView();
                }
            }
        }
        catch (Exception e) {

        }
    }


    private void showAlertDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Alert!");
        String value = names.get(position);
        if(value.equals("")) value = numbers.get(position);

        builder.setMessage("Are you sure want to delete " + value + " from block history?");
        builder.setCancelable(true);

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteNumber(position);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }



}
