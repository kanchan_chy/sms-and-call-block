package com.itsolutionslab.smsandcallblock.receiver;

/**
 * Created by user on 3/7/2016.
 */
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.Constants;
import com.itsolutionslab.smsandcallblock.utility.SharedPrefUtil;

public class PhoneCallStateListener extends PhoneStateListener {

    private Context context;
    String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    private ITelephony telephonyService;
    String number = "", name = "", photo = "", date = "", time = "";
    ArrayList<String> blackListNumbers = new ArrayList<>();
    ArrayList<String>blackListNames = new ArrayList<>();
    ArrayList<String>blackListPhotos = new ArrayList<>();

    DataHandler dataHandler;


    public PhoneCallStateListener(Context context){
        this.context = context;
    }


    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        //SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(context);

        switch (state) {

            case TelephonyManager.CALL_STATE_RINGING:

                dataHandler = new DataHandler(context);
                HashMap<String, ArrayList<String>> hashBlackList = dataHandler.getAllCallBlackList();
                if(hashBlackList != null) {
                    blackListNumbers = hashBlackList.get(Constants.NUMBER);
                    blackListNames = hashBlackList.get(Constants.NAME);
                    blackListPhotos = hashBlackList.get(Constants.PHOTO);
                    boolean flag = false;
                    for (int i=0; i<blackListNumbers.size(); i++) {
                        if(blackListNumbers.get(i).equals(incomingNumber)) {
                            number = incomingNumber;
                            name = blackListNames.get(i);
                            photo = blackListPhotos.get(i);
                            flag = true;
                            break;
                        }
                    }

                    if(flag) {
                        // String block_number = prefs.getString("block_number", null);
                        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                        //Turn ON the mute
                        audioManager.setStreamMute(AudioManager.STREAM_RING, true);
                        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                        try {
                            //   Toast.makeText(context, "in"+block_number, Toast.LENGTH_LONG).show();
                            Class clazz = Class.forName(telephonyManager.getClass().getName());
                            Method method = clazz.getDeclaredMethod("getITelephony");
                            method.setAccessible(true);
                            ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);
                            //Checking incoming call number
                            //    System.out.println("Call "+block_number);
                            //telephonyService.silenceRinger();//Security exception problem
                            telephonyService = (ITelephony) method.invoke(telephonyManager);
                            telephonyService.silenceRinger();
                            telephonyService.endCall();
                            loadIntoDatabase();
                        } catch (Exception e) {
                            // Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                        }
                        //Turn OFF the mute
                        audioManager.setStreamMute(AudioManager.STREAM_RING, false);
                        SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(context);
                        boolean notifyCallBlockStatus = sharedPrefUtil.getNotifyCallBlockStatus();
                        if(notifyCallBlockStatus) {
                            sendNotification();
                        }
                    }
                }

                break;
            case PhoneStateListener.LISTEN_CALL_STATE:

        }
        super.onCallStateChanged(state, incomingNumber);
    }




    private void loadIntoDatabase() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int status = calendar.get(Calendar.AM_PM);
        String am_Pm_status;
        if(status == 0) am_Pm_status = "AM";
        else am_Pm_status = "PM";
        date = months[month] + " " + day + ", " + year;
        time = hour + ":" + minute + " " + am_Pm_status;
        dataHandler.addCallBlockList(name, number, photo, date, time);
    }

    private void sendNotification() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String msg = "XXXXXXXXXXX January 31, 2016";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("SMS Blocked")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setAutoCancel(true);

        // mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(100, mBuilder.build());
        Toast.makeText(context, "Notification showed", Toast.LENGTH_SHORT).show();
    }



}
