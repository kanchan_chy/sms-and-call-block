package com.itsolutionslab.smsandcallblock.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telecom.Call;

public class SharedPrefUtil {

	Context mContext;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor spEditor;

    private final String CALL_BLOCK_STATUS = "call_block_status";
    private final String SMS_BLOCK_STATUS = "sms_block_status";
    private final String NOTIFY_CALL_BLOCK_STATUS = "notify_call_block_status";
    private final String NOTIFY_SMS_BLOCK_STATUS = "notify_sms_block_status";


	public SharedPrefUtil(Context mContext)
	{
		super();
		this.mContext = mContext;
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
	}


    public void setCallBlockStatus (boolean callBlockStatus) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(CALL_BLOCK_STATUS, callBlockStatus);
        spEditor.commit();

    }

    public boolean getCallBlockStatus () {
        return sharedPreferences.getBoolean(CALL_BLOCK_STATUS, true);

    }

    public void setSmsBlockStatus (boolean smsBlockStatus) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(SMS_BLOCK_STATUS, smsBlockStatus);
        spEditor.commit();

    }

    public boolean getSmsBlockStatus () {
        return sharedPreferences.getBoolean(SMS_BLOCK_STATUS, true);

    }

    public void setNotifyCallBlockStatus (boolean notifyCallBlockStatus) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(NOTIFY_CALL_BLOCK_STATUS, notifyCallBlockStatus);
        spEditor.commit();

    }

    public boolean getNotifyCallBlockStatus () {
        return sharedPreferences.getBoolean(NOTIFY_CALL_BLOCK_STATUS, true);

    }

    public void setNotifySmsBlockStatus (boolean notifySmsBlockStatus) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(NOTIFY_SMS_BLOCK_STATUS, notifySmsBlockStatus);
        spEditor.commit();

    }

    public boolean getNotifySmsBlockStatus () {
        return sharedPreferences.getBoolean(NOTIFY_SMS_BLOCK_STATUS, true);

    }



}