package com.itsolutionslab.smsandcallblock.fragment;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.itsolutionslab.smsandcallblock.AddBlackListActivity;
import com.itsolutionslab.smsandcallblock.ContactListActivity;
import com.itsolutionslab.smsandcallblock.InboxNumbersActivity;
import com.itsolutionslab.smsandcallblock.LogNumbersActivity;
import com.itsolutionslab.smsandcallblock.R;
import com.itsolutionslab.smsandcallblock.adapter.BlackListAdapter;
import com.itsolutionslab.smsandcallblock.db.DataHandler;
import com.itsolutionslab.smsandcallblock.utility.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by ASUS on 1/15/2016.
 */
public class CallBlackListFragment extends Fragment implements View.OnClickListener{

    static TextView txtEmpty;
    RecyclerView recyclerBlackList;
    FloatingActionButton fab;

    BlackListAdapter adapter;

    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;

    DataHandler dataHandler;

    ArrayList<String> names=new ArrayList<>();
    ArrayList<String>numbers=new ArrayList<>();
    ArrayList<String>photos=new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_call_black_list,container,false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtEmpty=(TextView)view.findViewById(R.id.txtEmpty);
        recyclerBlackList=(RecyclerView)view.findViewById(R.id.recyclerBlackList);
        fab=(FloatingActionButton)view.findViewById(R.id.fab);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerBlackList.setLayoutManager(layoutManager);

        fab.setOnClickListener(this);

        dataHandler = new DataHandler(getActivity());

        recyclerBlackList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                    onHide();
                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                    onShow();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }

                if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
                    scrolledDistance += dy;
                }

            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();
        prepareListItems();
        if(numbers.size()>0){
            txtEmpty.setVisibility(View.GONE);
            adapter=new BlackListAdapter(getActivity(), names, numbers, photos, Constants.OPTION_CALL);
            recyclerBlackList.setAdapter(adapter);
        }
        else {
            showEmptyView();
        }
    }

    public static void showEmptyView() {
        txtEmpty.setVisibility(View.VISIBLE);
    }


    private void onHide(){
        fab.setVisibility(View.GONE);
    }

    private void onShow(){
        fab.setVisibility(View.VISIBLE);
    }

    private void prepareListItems()
    {
        names=new ArrayList<>();
        numbers=new ArrayList<>();
        photos = new ArrayList<>();
      /*  for (int i=0; i < 10; i++) {
            names.add("Name"+(i+1));
            numbers.add("Number"+(i+1));
        }  */
        try {
            HashMap<String, ArrayList<String>> hashBlackList = dataHandler.getAllCallBlackList();
            if(hashBlackList != null) {
                names = hashBlackList.get(Constants.NAME);
                numbers = hashBlackList.get(Constants.NUMBER);
                photos = hashBlackList.get(Constants.PHOTO);
            }
        }
        catch (Exception e1) {

        }
    }


    private void showOptionListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_list, null);
        builder.setView(view);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();

        TextView txtAlertTitle = (TextView)view.findViewById(R.id.txtAlertTitle);
        TextView txtAlertLog = (TextView)view.findViewById(R.id.txtLog);
        TextView txtAlertSms = (TextView)view.findViewById(R.id.txtSMS);
        ImageButton imgBtnClose = (ImageButton)view.findViewById(R.id.imgBtnClose);

        txtAlertTitle.setText("Add contact for blocking call");

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtAlertLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent in = new Intent(getActivity(), LogNumbersActivity.class);
                        in.putExtra("from", Constants.OPTION_CALL);
                        startActivity(in);
                        alertDialog.dismiss();
                    }
                }, 100);
            }
        });

        txtAlertSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent in = new Intent(getActivity(), InboxNumbersActivity.class);
                        in.putExtra("from", Constants.OPTION_CALL);
                        startActivity(in);
                        alertDialog.dismiss();
                    }
                }, 100);
            }
        });

        alertDialog.show();

    }



    private void sendNotification() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getActivity(), notification);
        r.play();

        NotificationManager mNotificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        String msg = "XXXXXXXXXXX January 31, 2016";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("SMS Blocked")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setAutoCancel(true);

        // mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(100, mBuilder.build());
        Toast.makeText(getActivity(), "Notification showed", Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.fab)
        {
            showOptionListDialog();
          /*  Intent in = new Intent(getActivity(), AddBlackListActivity.class);
            in.putExtra("from", Constants.OPTION_CALL);
            startActivity(in);   */
        }
    }


}
