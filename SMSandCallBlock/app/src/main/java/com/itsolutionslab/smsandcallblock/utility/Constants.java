package com.itsolutionslab.smsandcallblock.utility;

/**
 * Created by ASUS on 1/26/2016.
 */
public class Constants {

    public static final String OPTION_CALL = "call";
    public static final String OPTION_SMS = "sms";
    public static final String CALL_INCOMING = "incoming";
    public static final String CALL_OUTGOING = "outgoing";
    public static final String CALL_MISSED = "missed";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String NUMBER = "number";
    public static final String PHOTO = "photo";
    public static final String DATE = "date";
    public static final String TIME = "time";


}
