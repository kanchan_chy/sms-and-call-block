package com.android.internal.telephony;

/**
 * Created by user on 3/1/2016.
 */
public abstract interface ITelephony {


    boolean endCall();


    void answerRingingCall();


    void silenceRinger();

}
